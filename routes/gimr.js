const express = require('express');
const router = express.Router();

const gimnasiaController = require('../controllers/gimnastas');

router.get('/', gimnasiaController.gimnasta_list);

module.exports=router;