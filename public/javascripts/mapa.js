const map = L.map('map-template').setView([-0.1865938,-78.5706258], 13);

L.tileLayer('https://c.tile.openstreetmap.org/${z}/${x}/${y}.png').addTo(map);

L.marker([-0.0794179,-78.4654358]).addTo(map);
L.marker([-0.1761841,-78.4815248]).addTo(map);
L.marker([-0.1029327,-78.4926028]).addTo(map);
