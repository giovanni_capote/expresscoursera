const Gimnastas = function(id,nombre,edad,nivel,horario){
    this.id = id;
    this.nombre = nombre;
    this.edad=edad;
    this.nivel=nivel;
    this.horario = horario;
}

Gimnastas.prototype.toString = function(){
    return "id: "+this.id+"Nombre: "+this.nombre+"Edad: "+this.edad;
}

Gimnastas.allgimnastas =[];
Gimnastas.add= function(aGim){
    Gimnastas.allgimnastas.push(aGim);
}

var a= new Gimnastas(1,'lisset',12,3,2);
var b= new Gimnastas(1,'Camila',12,3,2);

Gimnastas.add(a);
Gimnastas.add(b);

module.exports=Gimnastas;