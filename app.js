;
const express = require('express');
const cookieParser = require('cookie-parser');
const path = require('path');


var indexRouter = require('./routes/index');
var gimrRouter = require('./routes/gimr');

const app = express();





app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/gimnastas', gimrRouter);



//app.use(express.static(path.join(__dirname, 'public')));

app.listen(3000,()=>{
  console.log('Listen on port 3000')
})





